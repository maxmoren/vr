#ifndef _VR_H
#define _VR_H

#include <stdio.h>
#include <stdlib.h>

enum
{
	VR_ERROR,
	VR_OK
};

#define VR_MOUSE_BUTTON_LEFT 1<<0
#define VR_MOUSE_BUTTON_RIGHT 1<<1
#define VR_MOUSE_BUTTON_MIDDLE 1<<3
#define VR_MOUSE_BUTTON_WHEELUP 1<<4
#define VR_MOUSE_BUTTON_WHEELDOWN 1<<5

int vr_init(void);
void vr_grab_cursor(void);
void vr_release_cursor(void);
int vr_main_loop(void (*call_func)(void *), void (*mouse_func)(int, int, long, void *), void *user_data);
void vr_exit(int exit_code);

#if defined(DEBUG)
#define vr_debug(format, ...) \
	do { printf("%7s:%-3d -- %s(): " format, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__); } while(0)
#else
#define vr_debug(format, ...) do { } while(0)
#endif

#define vr_error(format, ...) \
	do { fprintf(stderr, "%7s:%-3d -- %s():\e[31m " format "\e[0m", __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__); } while(0)

#define vr_error_exit(format, ...) \
	do { fprintf(stderr, "%7s:%-3d -- %s():\e[31m " format "\e[0m", __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__); vr_exit(EXIT_FAILURE); } while(0)

#endif
