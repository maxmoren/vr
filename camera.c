#include <vr/vr.h>
#include <vr/vr_memory.h>
#include <vr/vr_vec2.h>
#include <vr/vr_pid.h>
#include <vr/vr_camera.h>

struct vr_camera
{
	struct vr_vec2 dest; /* Camera destination in world coordinates. */
	struct vr_vec2 pos;  /* Camera center in world coordinates. */
	struct vr_pid *pid_x;  /* PID controller used to adjust pos towards dest. */
	struct vr_pid *pid_y;  /* PID controller used to adjust pos towards dest. */
	double zoom;         /* Zoom level given in world units to screen size ratio. */
//	double speed;        /* Camera maximum panning velocity at zoom 1.0 in world units per second. */
	double t0;
};

struct vr_camera *vr_camera_new(void) {
	struct vr_camera *cam = vr_memory_alloc(1, sizeof(struct vr_camera));

	cam->pos.x = 0.0;
	cam->pos.y = 0.0;
	cam->dest.x = 0.0;
	cam->dest.y = 0.0;
	cam->zoom = 1.0;
	cam->t0 = SDL_GetTicks();
//	cam->speed = 100.0;

	cam->pid_x = vr_pid_new();
	cam->pid_y = vr_pid_new();

	vr_pid_set(cam->pid_x, 1.0, 0.0, -0.5);
	vr_pid_set(cam->pid_y, 1.0, 0.0, -0.5);

	return cam;
}

void vr_camera_move(struct vr_camera *cam, double dx, double dy) {
	cam->dest.x += dx;
	cam->dest.y += dy;
}

void vr_camera_set_dest(struct vr_camera *cam, double x, double y) {
	cam->dest.x = x;
	cam->dest.y = y;
}

void vr_camera_zoom(struct vr_camera *cam, double dzoom) {
	cam->zoom += dzoom;
}

// void vr_camera_set_zoom(struct vr_camera *cam, double zoom) {
//	cam->zoom = zoom;
// }

struct vr_camera_viewport vr_camera_to_viewport(struct vr_camera *cam, int w, int h) {
	struct vr_camera_viewport viewport;

	double ratio = 1.0;
//	double sz_min = w < h ? w : h;
	double sz_max = w > h ? w : h;

#if 0
	viewport.left   = (-cam->zoom / 2.0) * ratio;
	viewport.right  = (+cam->zoom / 2.0) * ratio;
	viewport.top    = (+cam->zoom / 2.0) * ratio;
	viewport.bottom = (-cam->zoom / 2.0) * ratio;
#endif

	viewport.left = (cam->pos.x / (double)w) * cam->zoom - cam->zoom / 2.0;
	viewport.right = (cam->pos.x / (double)w) * cam->zoom + cam->zoom / 2.0;
	viewport.bottom = (cam->pos.y / (double)h) * cam->zoom + cam->zoom / 2.0;
	viewport.top = (cam->pos.y / (double)h) * cam->zoom - cam->zoom / 2.0;

	return viewport;
}

void vr_camera_step(struct vr_camera *cam, double t) {
	double dt = t - cam->t0;

	cam->pos.x += vr_pid_get_output(cam->pid_x, cam->dest.x - cam->pos.x, dt);
	cam->pos.y += vr_pid_get_output(cam->pid_y, cam->dest.y - cam->pos.y, dt);
	cam->t0 = t;
}

void vr_camera_destroy(struct vr_camera *cam) {
	vr_pid_destroy(cam->pid_x);
	vr_pid_destroy(cam->pid_y);
	vr_memory_free(cam);
}