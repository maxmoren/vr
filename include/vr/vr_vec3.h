#ifndef _VR_VEC3_H
#define _VR_VEC3_H

struct vr_vec3
{
	float x, y, z;
};

struct vr_vec3 vr_vec3_add(struct vr_vec3 u, struct vr_vec3 v);
struct vr_vec3 vr_vec3_sub(struct vr_vec3 u, struct vr_vec3 v);
struct vr_vec3 vr_vec3_mul(struct vr_vec3 u, struct vr_vec3 v);
struct vr_vec3 vr_vec3_div(struct vr_vec3 u, struct vr_vec3 v);
struct vr_vec3 vr_vec3_abs(struct vr_vec3 u);
struct vr_vec3 vr_vec3_cross(struct vr_vec3 u, struct vr_vec3 v);
struct vr_vec3 vr_vec3_norm(struct vr_vec3 u);
struct vr_vec3 vr_vec3_round(struct vr_vec3 u);
float vr_vec3_len(struct vr_vec3 u);
float vr_vec3_dot(struct vr_vec3 u, struct vr_vec3 v);

#endif
