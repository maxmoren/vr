# <pep8-80 compliant>


bl_info = {
    "name": "VR mesh format (.vrm)",
    "author": "Max Morén",
    "version": (0, 2),
    "blender": (2, 5, 7),
    "location": "File > Export > VR Mesh (.vrm) ",
    "description": "Export VR Mesh",
    "warning": "",
#   "wiki_url": "http://wiki.blender.org/index.php/Extensions:2.5/Py/"
#               "Scripts/Import-Export/Raw_Mesh_IO",
#   "tracker_url": "https://projects.blender.org/tracker/index.php?"
#                  "func=detail&aid=25692",
    "category": "Import-Export"}

if "bpy" in locals():
    import imp
    if "import_raw" in locals():
        imp.reload(import_raw)
    if "export_raw" in locals():
        imp.reload(export_raw)
else:
    import bpy

from bpy.props import StringProperty, BoolProperty

class VrmExporter(bpy.types.Operator):
    '''Save Raw triangle mesh data'''
    bl_idname = "export_mesh.vrm"
    bl_label = "Export VRM"

    filepath = StringProperty(
            subtype='FILE_PATH',
            )

    def execute(self, context):
        from . import export_vrm
        export_vrm.write(self.filepath)
        return {'FINISHED'}

    def invoke(self, context, event):
        if not self.filepath:
            self.filepath = bpy.path.ensure_ext(bpy.data.filepath, ".vrm")
        wm = context.window_manager
        wm.fileselect_add(self)
        return {'RUNNING_MODAL'}


def menu_export(self, context):
    self.layout.operator(VrmExporter.bl_idname, text="VRM Faces (.vrm)")


def register():
    bpy.utils.register_module(__name__)
    bpy.types.INFO_MT_file_export.append(menu_export)


def unregister():
    bpy.utils.unregister_module(__name__)
    bpy.types.INFO_MT_file_export.remove(menu_export)

if __name__ == "__main__":
    register()
