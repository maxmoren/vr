#include <vr/vr.h>
#include <vr/vr_program.h>
#include <vr/vr_target.h>
#include <vr/vr_pass.h>
#include <vr/vr_texture.h>
#include <vr/vr_vec2.h>
#include <vr/vr_quaternion.h>
#include <vr/vr_arcball.h>
#include <vr/vr_camera.h>

#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef HAVE_GLES
#include <GLES2/gl2.h>
#else
#include <GL/glew.h>
#include <GL/gl.h>
#endif

static struct vr_camera *camera;
static int mouse_x = 0, mouse_y = 0;

void draw_lines() {
	glBegin(GL_LINES);
		glColor3d(1.0, 0.0, 0.0);
		glVertex2d(0.0, 0.0);
		glVertex2d(1.0, 0.0);

		glColor3d(0.0, 1.0, 0.0);
		glVertex2d(0.0, 0.0);
		glVertex2d(-1.0, 0.0);

		glColor3d(0.0, 0.0, 1.0);
		glVertex2d(0.0, 0.0);
		glVertex2d(0.0, 1.0);

		glColor3d(0.0, 1.0, 1.0);
		glVertex2d(0.0, 0.0);
		glVertex2d(0.0, -1.0);
	glEnd();
}

void draw() {
	vr_camera_step(camera, SDL_GetTicks());

	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	struct vr_camera_viewport vp;

	vp = vr_camera_to_viewport(camera, 512, 512);
//	vr_debug("left: %lf\n", vp.left);

	glOrtho(vp.left, vp.right, vp.bottom, vp.top, 0, 1);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	draw_lines();
}

void setup() {
	camera = vr_camera_new();

	vr_grab_cursor();
}

void mouse(int x, int y, long buttons, void *user_data) {

	if (buttons & VR_MOUSE_BUTTON_WHEELUP)
		vr_camera_zoom(camera, -.1);
	if (buttons & VR_MOUSE_BUTTON_WHEELDOWN)
		vr_camera_zoom(camera, +.1);

	vr_camera_move(camera, (double)x, (double)y);
}

int main(void) {
	vr_init();

	setup();

	vr_main_loop(&draw, &mouse, NULL);
	vr_exit(EXIT_SUCCESS);

	return EXIT_SUCCESS;
}
