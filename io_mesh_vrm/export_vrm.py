# <pep8-80 compliant>

"""
This script exports a Mesh to a VRM mesh format file.

The format is simply the vertex coordinates, vertex normals and texture
coordinates for each vertex, as ASCII, separated by newline characters for each
new vertex.

Usage:
Execute this script from the "File->Export" menu. You can select whether
modifiers should be applied.

"""

import bpy

def faceValues(face, mesh, matrix):
    co = []
    normal = []
    for verti in face.vertices:
        co.append((mesh.vertices[verti].co)[:])
        normal.append((mesh.vertices[verti].normal)[:])
    return (co, normal)


def faceToLine(face):
    return " ".join([("%.6f %.6f %.6f" % c) for c in face.co]
                  + [("%.6f %.6f %.6f" % n) for n in face.normal] + ["\n"])


def write(filepath):
    scene = bpy.context.scene

    faces = []
    for obj in bpy.context.selected_objects:
        is_tmp_mesh = False

        matrix = obj.matrix_world.copy()
        #for face in obj.data.faces:
        #    (co, normal) = faceValues(face, obj.data, matrix)
        #    faces.append((co, normal))

    # write the faces to a file
    file = open(filepath, "w")
    for face in obj.data.faces:
        for vertex in face.vertices:
            file.write(faceToLine(face.vertices[vertex]))
    file.close()
