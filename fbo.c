#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>

struct fbo
{
	GLuint texture;
	GLuint framebuffer;
	GLuint depth;
};

void fbo_bind(struct fbo *desc)
{
	glBindFramebuffer(GL_FRAMEBUFFER_EXT, desc->framebuffer);
}

void fbo_unbind(struct fbo *desc)
{
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
}

GLuint fbo_depth_texture(struct fbo *desc)
{
	return desc->depth;
}

void fbo_draw(struct fbo *desc, GLint x, GLint y, GLuint w, GLuint h)
{
    glPushAttrib(GL_VIEWPORT_BIT | GL_ENABLE_BIT);

    glViewport(x, y, w, h);

    glDisable(GL_CULL_FACE);
	glDisable(GL_LIGHTING);
	glEnable(GL_TEXTURE_2D);
	glDisable(GL_DEPTH_TEST);

	glColor4f(1.f, 1.f, 1.f, 1.f);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0.0, 1.0, 1.0, 0.0, -1.0, 1.0);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

    glBindTexture(GL_TEXTURE_2D, desc->texture);
	glBegin(GL_QUADS);
		vr_model_quad(1.0);
	
	glTexCoord2f(0.f, 1.f); glVertex2f(0.0f, 0.f);
	glTexCoord2f(0.f, 0.f); glVertex2f(0.0f, 1.f);
	glTexCoord2f(1.f, 0.f); glVertex2f(1.0f, 1.0f);
	glTexCoord2f(1.f, 1.f); glVertex2f(1.0f, 0.f);

	glEnd();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

    glPopAttrib();
}

void fbo_draw_depth(struct fbo *desc, GLint x, GLint y, GLuint w, GLuint h)
{
    glPushAttrib(GL_VIEWPORT_BIT | GL_ENABLE_BIT);
    glDisable(GL_CULL_FACE);
    glViewport(x, y, w, h);
	glPushMatrix();
	glDisable(GL_LIGHTING);
	glEnable(GL_TEXTURE_2D);
	glDisable(GL_DEPTH_TEST);
	glColor4f(1.f, 1.f, 1.f, 1.f);
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0.0, 1.0, 1.0, 0.0, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
//	glPushMatrix();
	glLoadIdentity();
    glBindTexture(GL_TEXTURE_2D, desc->depth);
	glBegin(GL_QUADS);
	glTexCoord2f(0.f, 1.f); glVertex2f(0.0f, 0.f);
	glTexCoord2f(0.f, 0.f); glVertex2f(0.0f, 1.f);
	glTexCoord2f(1.f, 0.f); glVertex2f(1.0f, 1.0f);
	glTexCoord2f(1.f, 1.f); glVertex2f(1.0f, 0.f);
	glEnd();
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
//	glPopMatrix();
	glPopMatrix();
    glPopAttrib();
}

struct fbo *fbo_create(GLuint w, GLuint h)
{
	struct fbo *desc;

	if ((desc = malloc(sizeof(struct fbo))) == NULL)
	{
		perror("malloc");
		return NULL;
	}

#if 0
	/* Create a texture for color data. */
    glGenTextures(1, &desc->texture);
    glBindTexture(GL_TEXTURE_2D, desc->texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glBindTexture(GL_TEXTURE_2D, 0);


	/* Create a renderbuffer for depth data. */
	glGenBuffers(1, &desc->depth);
	glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, desc->depth);
	glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT, w, h);
	glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, 0);
#endif
	glGenTextures(1, &desc->depth);
	glBindTexture(GL_TEXTURE_2D, desc->depth);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

	/* Special stuff. */
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
	glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, w, h, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);

	/* Create a FBO. */
  	glGenFramebuffersEXT(1, &desc->framebuffer);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, desc->framebuffer);

	/* Attach the texture to the color attachment point of FBO. */
//	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, desc->texture, 0);

	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_TEXTURE_2D, desc->depth, 0);

	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);

#if 0
	/* Attach the renderbuffer to the depth attachment point of FBO. */
	glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, desc->depth);
#endif

	GLenum err = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);

	if (err != GL_FRAMEBUFFER_COMPLETE_EXT)
	{
		fprintf(stderr, "Could not create FBO: %s\n", gluErrorString(glGetError()));

		free(desc);
		return NULL;
	}

//	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClear(GL_DEPTH_BUFFER_BIT);

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	return desc;
}

