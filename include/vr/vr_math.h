#ifndef _VR_MATH_H
#define _VR_MATH_H

#define sqr(x) ((x)*(x))

float vr_math_rad_to_deg(float rad);
float vr_math_deg_to_rad(float deg);

#endif
