#include <vr/vr.h>
#include <vr/vr_program.h>
#include <vr/vr_target.h>
#include <vr/vr_pass.h>
#include <vr/vr_texture.h>
#include <vr/vr_vec2.h>
#include <vr/vr_quaternion.h>
#include <vr/vr_arcball.h>

#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef HAVE_GLES
#include <GLES2/gl2.h>
#else
#include <GL/glew.h>
#include <GL/gl.h>
#endif

struct vr_program *raycast_program;
struct vr_target *target1, *target2;
struct vr_pass *pass1, *pass2;
struct vr_texture *volume_texture;

GLfloat rotation_matrix[16] = {
		1.0, 0.0, 0.0, 0.0,
		0.0, 1.0, 0.0, 0.0,
		0.0, 0.0, 1.0, 0.0,
		0.0, 0.0, 0.0, 1.0
};

GLint uniform_inside, uniform_outside, uniform_volume;

#define DATA_SET    "walnut.raw"
#define DATA_SIZE_X 128
#define DATA_SIZE_Y 96
#define DATA_SIZE_Z 114

void draw_cube(int outside)
{
	glPushAttrib(GL_TRANSFORM_BIT | GL_CURRENT_BIT | GL_ENABLE_BIT);

	glEnable(GL_CULL_FACE);
	if (outside)
		glCullFace(GL_BACK);
	else
		glCullFace(GL_FRONT);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluPerspective(65.0, 1.0, 0.01, 1000.0);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glTranslatef(0.0, 0.0, -3.0);

	float max_side = fmax(DATA_SIZE_X, fmax(DATA_SIZE_Y, DATA_SIZE_Z));
	glMultMatrixf(rotation_matrix);

	glScalef(((float)DATA_SIZE_X / max_side),
		     ((float)DATA_SIZE_Y / max_side),
		     ((float)DATA_SIZE_Z / max_side));

	glBegin(GL_QUADS);
    glColor3f(1.0, 1.0, 0.0); glVertex3f( 1.0f, 1.0f,-1.0f);	// Top Right Of The Quad (Top)
    glColor3f(0.0, 1.0, 0.0); glVertex3f(-1.0f, 1.0f,-1.0f);	// Top Left Of The Quad (Top)
    glColor3f(0.0, 1.0, 1.0); glVertex3f(-1.0f, 1.0f, 1.0f);	// Bottom Left Of The Quad (Top)
    glColor3f(1.0, 1.0, 1.0); glVertex3f( 1.0f, 1.0f, 1.0f);	// Bottom Right Of The Quad (Top)

    glColor3f(1.0, 0.0, 1.0); glVertex3f( 1.0f,-1.0f, 1.0f);	// Top Right Of The Quad (Bottom)
    glColor3f(0.0, 0.0, 1.0); glVertex3f(-1.0f,-1.0f, 1.0f);	// Top Left Of The Quad (Bottom)
    glColor3f(0.0, 0.0, 0.0); glVertex3f(-1.0f,-1.0f,-1.0f);	// Bottom Left Of The Quad (Bottom)
    glColor3f(1.0, 0.0, 0.0); glVertex3f( 1.0f,-1.0f,-1.0f);	// Bottom Right Of The Quad (Bottom)

    glColor3f(1.0, 1.0, 1.0); glVertex3f( 1.0f, 1.0f, 1.0f);	// Top Right Of The Quad (Front)
    glColor3f(0.0, 1.0, 1.0); glVertex3f(-1.0f, 1.0f, 1.0f);	// Top Left Of The Quad (Front)
    glColor3f(0.0, 0.0, 1.0); glVertex3f(-1.0f,-1.0f, 1.0f);	// Bottom Left Of The Quad (Front)
    glColor3f(1.0, 0.0, 1.0); glVertex3f( 1.0f,-1.0f, 1.0f);	// Bottom Right Of The Quad (Front)

    glColor3f(1.0, 0.0, 0.0); glVertex3f( 1.0f,-1.0f,-1.0f);	// Top Right Of The Quad (Back)
    glColor3f(0.0, 0.0, 0.0); glVertex3f(-1.0f,-1.0f,-1.0f);	// Top Left Of The Quad (Back)
    glColor3f(0.0, 1.0, 0.0); glVertex3f(-1.0f, 1.0f,-1.0f);	// Bottom Left Of The Quad (Back)
    glColor3f(1.0, 1.0, 0.0); glVertex3f( 1.0f, 1.0f,-1.0f);	// Bottom Right Of The Quad (Back)

    glColor3f(0.0, 1.0, 1.0); glVertex3f(-1.0f, 1.0f, 1.0f);	// Top Right Of The Quad (Left)
    glColor3f(0.0, 1.0, 0.0); glVertex3f(-1.0f, 1.0f,-1.0f);	// Top Left Of The Quad (Left)
    glColor3f(0.0, 0.0, 0.0); glVertex3f(-1.0f,-1.0f,-1.0f);	// Bottom Left Of The Quad (Left)
    glColor3f(0.0, 0.0, 1.0); glVertex3f(-1.0f,-1.0f, 1.0f);	// Bottom Right Of The Quad (Left)

    glColor3f(1.0, 1.0, 0.0); glVertex3f( 1.0f, 1.0f,-1.0f);	// Top Right Of The Quad (Right)
    glColor3f(1.0, 1.0, 1.0); glVertex3f( 1.0f, 1.0f, 1.0f);	// Top Left Of The Quad (Right)
    glColor3f(1.0, 0.0, 1.0); glVertex3f( 1.0f,-1.0f, 1.0f);	// Bottom Left Of The Quad (Right)
    glColor3f(1.0, 0.0, 0.0); glVertex3f( 1.0f,-1.0f,-1.0f);	// Bottom Right Of The Quad (Right)
  	glEnd();

  	glPopMatrix();
  	glMatrixMode(GL_PROJECTION);
  	glPopMatrix();

  	glPopAttrib();
}

void draw_quad()
{
	glBegin(GL_QUADS);
		glTexCoord2f(0.0, 0.0); glVertex2f(-1.0, -1.0);
		glTexCoord2f(1.0, 0.0); glVertex2f(+1.0, -1.0);
		glTexCoord2f(1.0, 1.0); glVertex2f(+1.0, +1.0);
		glTexCoord2f(0.0, 1.0); glVertex2f(-1.0, +1.0);
	glEnd();
}

void draw_inside(void *user_data)
{
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	draw_cube(0);
}

void draw_outside(void *user_data)
{
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	draw_cube(1);
}

void draw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	vr_pass_render(pass1);
	vr_pass_render(pass2);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glColor4f(1.0, 1.0, 1.0, 1.0);

	glActiveTexture(GL_TEXTURE0 + 0);
	glBindTexture(GL_TEXTURE_2D, vr_target_get_object(target1));

	glActiveTexture(GL_TEXTURE0 + 1);
	glBindTexture(GL_TEXTURE_2D, vr_target_get_object(target2));
	
	glActiveTexture(GL_TEXTURE0 + 2);
	glBindTexture(GL_TEXTURE_3D, vr_texture_get_object(volume_texture));

	vr_program_enable(raycast_program);

	glUniform1i(uniform_inside, 0);
	glUniform1i(uniform_outside, 1);
	glUniform1i(uniform_volume, 2);

	draw_quad();
	
	vr_program_disable();
}

void setup() {
	vr_debug("loading dataset...\n");

	vr_arcball_init(512, 512);

	/* Load 3D texture data. */
	volume_texture = vr_texture_3d_new(DATA_SET, DATA_SIZE_X, DATA_SIZE_Y, DATA_SIZE_Z, GL_ALPHA16, GL_ALPHA, GL_UNSIGNED_SHORT);

	vr_debug("dataset loaded\n");

	/* Load the programs. */
	raycast_program = vr_program_new("glsl/passthrough.vert", "glsl/raycast.frag");

	vr_debug("shaders loaded\n");

	uniform_inside = vr_program_get_uniform(raycast_program, "inside");
	uniform_outside = vr_program_get_uniform(raycast_program, "outside");
	uniform_volume = vr_program_get_uniform(raycast_program, "volume");

	/* Linearly interpolate colors. */
	glShadeModel(GL_SMOOTH);
	glEnable(GL_BLEND);
	// glEnable(GL_MULTISAMPLE);

	if (raycast_program == NULL)
		exit(EXIT_FAILURE);

	/* Set up render passes. */
	pass1 = vr_pass_new(512, 512, draw_inside, NULL);
	pass2 = vr_pass_new(512, 512, draw_outside, NULL);

	target1 = vr_target_new_texture(GL_COLOR_ATTACHMENT0, 512, 512, GL_NEAREST, GL_NEAREST);
	target2 = vr_target_new_texture(GL_COLOR_ATTACHMENT0, 512, 512, GL_NEAREST, GL_NEAREST);
	vr_pass_attach_target(pass1, target1);
	vr_pass_attach_target(pass2, target2);
}

void mouse(int dx, int dy, long buttons, void *user_data) {
	static int pressed = 0;
	static int x = 0, y = 0;

	x += dx;
	y += dy;

	if (buttons & VR_MOUSE_BUTTON_LEFT) {
		if (pressed == 0) {
			vr_arcball_click(x, y);
			pressed = 1;
		}
		else {
			struct vr_quat new_rotation;
			vr_arcball_get_quaternion(x, y, &new_rotation);

			GLfloat temp_matrix[16] = {
					1.0f, 0.0f, 0.0f, 0.0f,
					0.0f, 1.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 1.0f, 0.0f,
					0.0f, 0.0f, 0.0f, 1.0f
				};
			GLfloat new_rotation_matrix[16] = {
					1.0f, 0.0f, 0.0f, 0.0f,
					0.0f, 1.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 1.0f, 0.0f,
					0.0f, 0.0f, 0.0f, 1.0f,
				};

			vr_quat_mat(new_rotation, new_rotation_matrix);
			vr_quat_next(temp_matrix, rotation_matrix, new_rotation_matrix);

			memcpy(rotation_matrix, temp_matrix, sizeof(GLfloat) * 16);
		}
	}
	else {
		pressed = 0;
	}
}

int main(void) {
	vr_init();

	setup();

	vr_main_loop(&draw, &mouse, NULL);
	vr_exit(EXIT_SUCCESS);

	return EXIT_SUCCESS;
}
