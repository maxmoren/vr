#include <vr/vr_quaternion.h>

#include <math.h>

struct vr_quat vr_quat_norm(struct vr_quat q)
{
	struct vr_quat r;

	double len = sqrt(q.w*q.w + q.x*q.x + q.y*q.y + q.z*q.z);

	r.w = q.w / len;
	r.x = q.x / len;
	r.y = q.y / len;
	r.z = q.z / len;

	return r;
}

/*
struct vr_quat vr_quat_mul(struct vr_quat q, struct vr_quat r)
{
	struct vr_quat s;

	s.w = q.w*r.w - q.x*r.x - q.y*r.y - q.z*r.z;
	s.x = q.w*r.x + q.x*r.w + q.y*r.z - q.z*r.y;
	s.y = q.w*r.y + q.y*r.w + q.z*r.x - q.x*r.z;
	s.z = q.w*r.z + q.z*r.w + q.z*r.y - q.y*r.x;

	return s;
}
*/
void vr_quat_mat(struct vr_quat q, GLfloat *mat)
{
  GLfloat x2 = q.x*q.x;
  GLfloat y2 = q.y*q.y;
  GLfloat z2 = q.z*q.z;
  GLfloat xy = q.x*q.y;
  GLfloat xz = q.x*q.z;
  GLfloat yz = q.y*q.z;
  GLfloat wx = q.w*q.x;
  GLfloat wy = q.w*q.y;
  GLfloat wz = q.w*q.z;

  mat[0] = 1 - 2*y2 - 2*z2;
  mat[1] = 2*xy + 2*wz;
  mat[2] = 2*xz - 2*wy;

  mat[4] = 2*xy - 2*wz;
  mat[5] = 1 - 2*x2 - 2*z2;
  mat[6] = 2*yz + 2*wx;

  mat[8] = 2*xz + 2*wy;
  mat[9] = 2*yz - 2*wx;
  mat[10]= 1 - 2*x2 - 2*y2;

}

void vr_quat_next(GLfloat* dest, GLfloat* left, GLfloat* right)
{
  dest[0] = left[0]*right[0] + left[1]*right[4] + left[2] *right[8];
  dest[1] = left[0]*right[1] + left[1]*right[5] + left[2] *right[9];
  dest[2] = left[0]*right[2] + left[1]*right[6] + left[2] *right[10];
  dest[4] = left[4]*right[0] + left[5]*right[4] + left[6] *right[8];
  dest[5] = left[4]*right[1] + left[5]*right[5] + left[6] *right[9];
  dest[6] = left[4]*right[2] + left[5]*right[6] + left[6] *right[10];
  dest[8] = left[8]*right[0] + left[9]*right[4] + left[10]*right[8];
  dest[9] = left[8]*right[1] + left[9]*right[5] + left[10]*right[9];
  dest[10]= left[8]*right[2] + left[9]*right[6] + left[10]*right[10];
}
