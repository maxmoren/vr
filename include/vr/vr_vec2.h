#ifndef _VR_VEC2
#define _VR_VEC2

struct vr_vec2
{
	float x, y, z;
};

struct vr_vec2 vr_vec2_sub(struct vr_vec2 u, struct vr_vec2 v);
struct vr_vec2 vr_vec2_norm(struct vr_vec2 u);
float vr_vec2_len(struct vr_vec2 u);

#endif
