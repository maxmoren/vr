#include <vr/vr.h>
#include <vr/vr_vec3.h>
#include <vr/vr_quaternion.h>
#include <vr/vr_arcball.h>

#include <math.h>

static struct vr_vec3 arcball_p0;

float adjust_width;
float adjust_height;

#define ARCBALL_EPSILON 1.0e-5

static struct vr_vec3 arcball_map(int x, int y)
{
	struct vr_vec3 v;
	float tmp_x = (float)x, tmp_y = (float)y;

//	tmp_y = adjust_height - y - 1.0;

	tmp_x = 2.0f * (tmp_x / adjust_width) - 1.0f;
	tmp_y = 2.0f * (tmp_y / adjust_height) - 1.0f;

	tmp_y = -tmp_y;

	float length_squared = tmp_x * tmp_x + tmp_y * tmp_y;

	if (length_squared > 1.0f)
	{
		float norm = (1.0f / sqrtf(length_squared));

		v.x = tmp_x * norm;
		v.y = tmp_y * norm;
		v.z = 0.0f;
	}
	else
	{
		v.x = tmp_x;
		v.y = tmp_y;
		v.z = sqrtf(1.0f - length_squared);
	}

	return v;
}

void vr_arcball_init(int width, int height)
{
	adjust_width = (float)width;
	adjust_height = (float)height;
}

void vr_arcball_click(int x, int y)
{
	arcball_p0 = arcball_map(x, y);
}

void vr_arcball_get_quaternion(int x, int y, struct vr_quat *q)
{
	struct vr_vec3 p = arcball_map(x, y);
	struct vr_vec3 tmp_axis = vr_vec3_cross(arcball_p0, p);

	if (vr_vec3_len(tmp_axis) > ARCBALL_EPSILON)
	{
		q->x = tmp_axis.x;
		q->y = tmp_axis.y;
		q->z = tmp_axis.z;
		q->w = vr_vec3_dot(arcball_p0, p);

		*q = vr_quat_norm(*q);
	}
	else
	{
		q->x = 0.0;
		q->y = 0.0;
		q->z = 0.0;
		q->w = 0.0;
	}

	arcball_p0 = p;
}
