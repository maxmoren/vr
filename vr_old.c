#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <GL/glew.h>
#include <GL/glut.h>

#include <SDL/SDL.h>

#include "file.h"
#include "fbo.h"

#define SHADOW_MAP_W 512
#define SHADOW_MAP_H 512

#define SCREEN_W 700
#define SCREEN_H 700

static int angle_x = 0, angle_y = 0;
static GLuint texture;
static GLuint color_program = 0;

struct fbo *light_fbo;

static GLuint load_shader(const char *name, const GLchar *src, GLint src_len, GLuint type)
{
	GLuint shader = glCreateShader(type);

	glShaderSource(shader, 1, &src, &src_len);
	glCompileShader(shader);

	GLint status = GL_FALSE;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

	if (status != GL_TRUE)
	{
		char message[1024];

		memset(message, '\0', 1024);

		glGetShaderInfoLog(shader, 1024, NULL, message);
		glDeleteShader(shader);

		fprintf(stderr, "%s: Compiler %s", name, message);
		return 0;
	}

	return shader;
}

GLuint shadowMapUniform = 0;

void load_program(const char *vertex_path, const char *fragment_path, GLuint *target)
{
	GLuint program;
	struct file *vertex_file, *fragment_file;

	if ((vertex_file = file_load(vertex_path)) == NULL ||
	    (fragment_file = file_load(fragment_path)) == NULL)
		exit(EXIT_FAILURE);

	GLuint vshader = load_shader(vertex_path, file_ptr(vertex_file), file_size(vertex_file), GL_VERTEX_SHADER);
	GLuint fshader = load_shader(fragment_path, file_ptr(fragment_file), file_size(fragment_file), GL_FRAGMENT_SHADER);

	file_close(vertex_file);
	file_close(fragment_file);

	if (vshader == 0 || fshader == 0)
		return;

	if ((program = glCreateProgram()) == 0)
	{
		fprintf(stderr, "glCreateProgram: %s\n", gluErrorString(glGetError()));

		if (color_program == 0)
			exit(EXIT_FAILURE);
		else
			return;
	}

	glAttachShader(program, vshader);
	glAttachShader(program, fshader);
	glDeleteShader(vshader);
	glDeleteShader(fshader);

	glLinkProgram(program);

	GLint status = GL_FALSE;
	glGetProgramiv(program, GL_LINK_STATUS, &status);

	if (status != GL_TRUE)
	{
		char message[1024];
		GLsizei length = 0;

		glGetProgramInfoLog(program, 1024, &length, message);
		glDeleteProgram(program);

		fprintf(stderr, "Linker %s", message);
		return;
	}

	shadowMapUniform = glGetUniformLocationARB(program, "ShadowMap");

	if (*target != 0)
		glDeleteProgram(*target);

	*target = program;

	printf("Loaded program \"%s\", \"%s\".\n", vertex_path, fragment_path);
}

GLfloat lightPosition[] = { 3.0f, 3.0f, 3.0f, 0.0f };

void draw_objects()
{
	glMatrixMode(GL_MODELVIEW);

/*
	glPushMatrix();

	glTranslated(lightPosition[0], lightPosition[1], lightPosition[2]);
	glColor3d(1.0, 1.0, 0.0);
	glutSolidSphere(0.1, 10, 10);

	glPopMatrix();
 */

	glPushMatrix();
	{
		glRotated(angle_x, 0., 1., 0.);
		glRotated(angle_y, 0., 0., 1.);
		glColor3d(1.0, 0.0, 0.0);
		glutSolidCube(1.0);
	}
	glPopMatrix();

# if 0
	glPushMatrix();

	glTranslated(0.0, -1.0, 0);
	glTranslated(0.4, 0.0, 0.2);
	glColor3d(1.0, 0.0, 0.0);
	glutSolidCube(0.3);
	glTranslated(0.4, 0.0, 0.2);
	glColor3d(0.0, 0.0, 1.0);
	glutSolidCube(0.5);
	glTranslated(0.4, 0.0, 0.2);
	glColor3d(0.0, 1.0, 0.0);
	glutSolidCube(0.8);
	glPopMatrix();

	glTranslated(0.0, -1.0, 0);
	glScaled(5.0, 5.0, 5.0);
#endif

	glColor3d(0.3, 0.3, 0.3);
	glBegin(GL_QUADS);
		glVertex3d(-5, -0.5, -5);
		glVertex3d( 5, -0.5, -5);
		glVertex3d( 5, -0.5,  5);
		glVertex3d(-5, -0.5,  5);
	glEnd();
}

void draw(void)
{
	static float light_rotation = 0.0;

	light_rotation = (light_rotation > 360.0 ? light_rotation - 360.0 : light_rotation) + 0.01;
	lightPosition[0] = cos(light_rotation) * 3.0;
	lightPosition[2] = sin(light_rotation) * 3.0;

	/* Render scene into shadow map from light perspective. */

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(lightPosition[0], lightPosition[1], lightPosition[2],
	          0.0, 0.0, 0.0,
	          0.0, 1.0, 0.0);

	/* Stash away the light matrices for later use with texture matrix. */

	static double lightViewMatrix[16];
	static double lightProjectionMatrix[16];

	glGetDoublev(GL_PROJECTION_MATRIX, lightProjectionMatrix);
	glGetDoublev(GL_MODELVIEW_MATRIX, lightViewMatrix);

	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
	fbo_bind(light_fbo);
	{
		glViewport(0, 0, SHADOW_MAP_W, SHADOW_MAP_H);
		glClear(GL_DEPTH_BUFFER_BIT);

		glPolygonOffset(1.0f, 1.0f);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		glEnable(GL_POLYGON_OFFSET_FILL);

		draw_objects();

		glDisable(GL_CULL_FACE);
		glDisable(GL_POLYGON_OFFSET_FILL);
	}
	fbo_unbind(light_fbo);

	/* Prepare texture matrix for real scene render. */

	const GLdouble biasMatrix[16] = {
		0.5, 0.0, 0.0, 0.0, 
		0.0, 0.5, 0.0, 0.0,
		0.0, 0.0, 0.5, 0.0,
		0.5, 0.5, 0.5, 1.0
	};

	glMatrixMode(GL_TEXTURE);
	glActiveTextureARB(GL_TEXTURE7);
	glUniform1iARB(shadowMapUniform,7);
	glLoadIdentity();
	glLoadMatrixd(biasMatrix);
	glMultMatrixd(lightProjectionMatrix);
	glMultMatrixd(lightViewMatrix);

	/* Render real scene. */

	glViewport(0, 0, SCREEN_W, SCREEN_H);
	glMatrixMode(GL_MODELVIEW);
	glBindTexture(GL_TEXTURE_2D, fbo_depth_texture(light_fbo));

	glLoadIdentity();
	gluLookAt(0.0, 0.0, 5.0,
	          0.0, 0.0, 0.0,
	          0.0, 1.0, 0.0);

	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE); 
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(color_program);
	draw_objects();
	glUseProgram(0);

	fbo_draw_depth(light_fbo, 0, 0, 200, 200);
}

#if 0

void load_texture(void)
{
	char *data;
	int fd;
	struct stat stat;

	assert((fd = open("foot.raw", O_RDONLY)) >= 0);
	assert(fstat(fd, &stat) == 0);
	assert((data = mmap(NULL, stat.st_size, PROT_READ, MAP_PRIVATE, fd, 0)) != NULL);

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_3D, texture);

	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexImage3D(GL_TEXTURE_3D, 0, GL_LUMINANCE8, 256, 256, 256, 0,
		GL_LUMINANCE, GL_UNSIGNED_BYTE, data);
}

#endif

int main(int argc, char **argv)
{
	SDL_Surface *screen;

	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER);
	atexit(SDL_Quit);

	glutInit(&argc, argv);

	/* Initialize SDL. */

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
//	SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 24);
	SDL_GL_SetAttribute(SDL_GL_SWAP_CONTROL, 1);
#if 0
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
#endif

	if ((screen = SDL_SetVideoMode(SCREEN_W, SCREEN_H, 32, SDL_OPENGL)) == NULL)
	{
		fprintf(stderr, "error: SDL_SetVideoMode: %s\n", SDL_GetError());
		return EXIT_FAILURE;
	}

	GLenum err = glewInit();

	if (GLEW_OK != err)
	{
		fprintf(stderr, "error: %s\n", glewGetErrorString(err));
		return EXIT_FAILURE;
	}

	if (!GLEW_VERSION_2_1)
	{
		fprintf(stderr, "error: OpenGL version 2.1 or higher required.\n");
		return EXIT_FAILURE;
	}

	if (!GLEW_ARB_vertex_program || !GLEW_ARB_fragment_program)
	{
		fprintf(stderr, "error: Programmable shader support missing.\n");
		return EXIT_FAILURE;
	}

	load_program("color.vertex.glsl", "color.fragment.glsl", &color_program);
//	load_texture();

	/* Initialize OpenGL. */

	glClearColor(0.25, 0.25, 0.25, 1.0);
	glEnable(GL_DEPTH_TEST);
//	glEnable(GL_CULL_FACE);
	glMatrixMode(GL_PROJECTION);
	gluPerspective(45.0, ((double)SCREEN_W / (double)SCREEN_H), 1.0, 1000.0);
	glMatrixMode(GL_MODELVIEW);

	/* Lighting */

	GLfloat white[]   = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat ambient[] = { 0.1f, 0.1f, 0.1f, 1.0f };
	GLfloat diffuse[] = { 0.3f, 0.3f, 0.3f, 1.0f };

    glShadeModel(GL_SMOOTH);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);
    glMaterialfv(GL_FRONT, GL_SPECULAR, white);
    glMaterialf(GL_FRONT, GL_SHININESS, 16.0f);
    glLightfv(GL_LIGHT1, GL_POSITION, lightPosition);
    glLightfv(GL_LIGHT1, GL_AMBIENT, ambient);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, diffuse);
    glLightfv(GL_LIGHT1, GL_SPECULAR, white);
    glEnable(GL_LIGHT1);
    glEnable(GL_LIGHTING);

	/* Create FBO */

	light_fbo = fbo_create(SHADOW_MAP_W, SHADOW_MAP_H);

	int down = 0;

	for (;;)
	{
		SDL_Event event;

		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
				return EXIT_SUCCESS;
			else if (event.type == SDL_KEYDOWN)
			{
				switch (event.key.keysym.sym)
				{
					case SDLK_ESCAPE: return EXIT_SUCCESS;
					case 'r': load_program("color.vertex.glsl", "color.fragment.glsl", &color_program); break;
				}
			}
			else if (down && event.type == SDL_MOUSEMOTION)
			{
				angle_x = (angle_x + event.motion.xrel) % 360;
				// angle_y = (angle_y + event.motion.yrel) % 360;
			}
            else if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == 1)
				down = 1;
			else if (event.type == SDL_MOUSEBUTTONUP && event.button.button == 1)
				down = 0;
		}

		draw();

		SDL_GL_SwapBuffers();
	}

	return EXIT_SUCCESS;
}

