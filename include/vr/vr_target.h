#ifndef _VR_TARGET_H
#define _VR_TARGET_H

#ifdef HAVE_GLES
#include <GLES2/gl2.h>
#else
#include <GL/glew.h>
#include <GL/gl.h>
#endif

enum
{
	VR_TARGET_TEXTURE,
	VR_TARGET_RENDERBUFFER
};

struct vr_target;

struct vr_target *vr_target_new_texture(GLint attachment, GLsizei width, GLsizei height, GLint min_filter, GLint mag_filter);
int vr_target_get_type(struct vr_target *target);
GLuint vr_target_get_object(struct vr_target *target);
GLenum vr_target_get_attachment(struct vr_target *target);

#endif
