#include <vr/vr.h>
#include <vr/vr_vec2.h>

inline struct vr_vec2 vr_vec2_norm(struct vr_vec2 u)
{
	struct vr_vec2 r;
	float n = vr_vec2_len(u);
	r.x = u.x / n;
	r.y = u.y / n;
	return r;
}

inline struct vr_vec2 vr_vec2_sub(struct vr_vec2 u, struct vr_vec2 v)
{
	struct vr_vec2 r;
	r.x = u.x - v.x;
	r.y = u.y - v.y;
	return r;
}


inline float vr_vec2_len(struct vr_vec2 u)
{
	return sqrt(sqr(u.x) + sqr(u.y));
}
