#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <vr/vr.h>
#include <vr/vr_memory.h>

void *vr_memory_alloc(unsigned size, unsigned count) {
	void *ptr;

	if ((ptr = malloc(size * count)) == NULL)
		vr_error_exit("malloc failed with %s\n", strerror(errno));

	return ptr;
}

void vr_memory_free(void *ptr) {
	free(ptr);
}