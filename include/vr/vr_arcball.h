#ifndef __VR_TRACKBALL_H
#define __VR_TRACKBALL_H

void vr_arcball_init(int x, int y);
void vr_arcball_click(int x, int y);
void vr_arcball_get_quaternion(int x, int y, struct vr_quat *q);

#endif
