#include <vr/vr_math.h>

#define PI 3.1415926536

inline float vr_math_rad_to_deg(float rad)
{
    return rad * (180.0 / PI);
}

inline float vr_math_deg_to_rad(float deg)
{
	return deg / (180.0 / PI);
}
