#include <vr/vr.h>
#include <vr/vr_file.h>
#include <vr/vr_memory.h>
#include <vr/vr_texture.h>
#include <vr/vr_gl.h>

#ifdef HAVE_GLES
#include <GLES2/gl2.h>
#else
#include <GL/glew.h>
#include <GL/gl.h>
#endif

struct vr_texture
{
    GLuint texture;
};

struct vr_texture *vr_texture_3d_new(const char *path, int width, int height, int depth, GLenum internal_format, GLenum stored_format, GLenum stored_type)
{
	struct vr_file *file = vr_file_new(path);
	struct vr_texture *texture = vr_memory_alloc(sizeof(struct vr_texture), 1);

	if (file == NULL)
		return NULL;

	glGenTextures(1, &texture->texture);
	glBindTexture(GL_TEXTURE_3D, texture->texture);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexImage3D(GL_TEXTURE_3D, 0, internal_format, width, height, depth, 0, stored_format, stored_type, vr_file_ptr(file));
    
    VR_GL_CHECK_ERROR("glTexImage3D");
    
    glBindTexture(GL_TEXTURE_3D, 0);

    vr_file_close(file);

    return texture;
}

GLuint vr_texture_get_object(struct vr_texture *texture)
{
    return texture->texture;
}
