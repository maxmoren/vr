#ifndef __VR_CAMERA_H
#define __VR_CAMERA_H

struct vr_camera_viewport
{
	double left, right, top, bottom; /* Left, right, top bottom. */
	struct vr_vec2 translate;        /* View translation. */
};

struct vr_camera *vr_camera_new(void);
void vr_camera_move(struct vr_camera *cam, double dx, double dy);
struct vr_camera_viewport vr_camera_to_viewport(struct vr_camera *cam, int w, int h);
void vr_camera_step(struct vr_camera *cam, double t);
void vr_camera_zoom(struct vr_camera *cam, double dzoom);

#endif
