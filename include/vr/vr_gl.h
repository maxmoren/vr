#ifndef _VR_GL_H
#define _VR_GL_H

const char *vr_gl_error(GLenum error_value);
static GLenum vr_gl_last_error = GL_NO_ERROR;

#define VR_GL_CHECK_ERROR(last_function_call) do { \
		if ((vr_gl_last_error = glGetError()) != GL_NO_ERROR) \
			vr_error(last_function_call "() failed with %s\n", vr_gl_error(vr_gl_last_error)); \
	} while(0)

#endif
