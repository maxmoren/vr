#ifndef _VR_MEMORY_H
#define _VR_MEMORY_H

void *vr_memory_alloc(unsigned size, unsigned count);
void vr_memory_free(void *ptr);

#endif
