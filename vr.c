#include <stdlib.h>
#include <GL/glew.h>
#include <SDL.h>
#define NO_SDL_GLEXT
#include <SDL_opengl.h>

//#if defined(HAVE_GLES)
//#include <GLES2/gl2.h>
// #else
// #include <GL/glew.h>
//#endif

#include <vr/vr.h>
#include <vr/vr_print.h>
#include <vr/vr_gl.h>
#include <vr/vr_program.h>

#define SCREEN_W (512)
#define SCREEN_H (512)

// static SDL_Window *main_window = NULL;
// static SDL_GLContext *main_context = NULL;

struct vr_program
{
	/* Vertex attribute locations. */
	GLint aPosition, aTexCoord, aNormal;
};

void vr_exit(int exit_code)
{
//	if (main_context != NULL)
//		SDL_GL_DeleteContext(main_context);

//	if (main_window != NULL)
//    	SDL_DestroyWindow(main_window);

	SDL_Quit();

	vr_debug("vr_exit called with exit_code %d\n", exit_code);

	exit(exit_code);
}

void vr_grab_cursor(void)
{
	SDL_ShowCursor(0);
	SDL_WM_GrabInput(SDL_GRAB_ON);
}

void vr_release_cursor(void)
{
	SDL_ShowCursor(1);
	SDL_WM_GrabInput(SDL_GRAB_OFF);
}

int vr_main_loop(void (*draw_func)(void *), void(*mouse_func)(int, int, long, void *), void *user_data)
{
	vr_debug("vr_main_loop called\n");

	static long down = 0;

	for (;;)
	{
		SDL_Event event;

		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
				return EXIT_SUCCESS;
			else if (event.type == SDL_KEYDOWN)
			{
				switch (event.key.keysym.sym)
				{
					case SDLK_ESCAPE:
						return VR_OK;
					case 'r':
						vr_program_reload_all(); break;
					default:
						break;
				}
			}
			else if (mouse_func && event.type == SDL_MOUSEMOTION)
			{
				mouse_func(event.motion.xrel, event.motion.yrel, down, user_data);
			}
            else if (mouse_func && event.type == SDL_MOUSEBUTTONDOWN) {
            	if (event.button.button == SDL_BUTTON_LEFT)
            		down |= VR_MOUSE_BUTTON_LEFT;
            	else if (event.button.button == SDL_BUTTON_MIDDLE)
					down |= VR_MOUSE_BUTTON_MIDDLE;
				else if (event.button.button == SDL_BUTTON_RIGHT)
					down |= VR_MOUSE_BUTTON_RIGHT;
				else if (event.button.button == SDL_BUTTON_WHEELUP)
					down |= VR_MOUSE_BUTTON_WHEELUP;
				else if (event.button.button == SDL_BUTTON_WHEELDOWN)
					down |= VR_MOUSE_BUTTON_WHEELDOWN;

				mouse_func(0, 0, down, user_data);
			}
			else if (mouse_func && event.type == SDL_MOUSEBUTTONUP) {
				if (event.button.button == SDL_BUTTON_LEFT)
            		down &= ~VR_MOUSE_BUTTON_LEFT;
            	else if (event.button.button == SDL_BUTTON_MIDDLE)
					down &= ~VR_MOUSE_BUTTON_MIDDLE;
				else if (event.button.button == SDL_BUTTON_RIGHT)
					down &= ~VR_MOUSE_BUTTON_RIGHT;
				else if (event.button.button == SDL_BUTTON_WHEELUP)
					down &= ~VR_MOUSE_BUTTON_WHEELUP;
				else if (event.button.button == SDL_BUTTON_WHEELDOWN)
					down &= ~VR_MOUSE_BUTTON_WHEELDOWN;

				mouse_func(0, 0, down, user_data);
			}
		}

		draw_func(user_data);

		SDL_GL_SwapBuffers();
//		SDL_GL_SwapWindow(main_window);
	}

	vr_debug("Leaving main loop\n");

	return VR_OK;
}

int vr_init(void)
{
	vr_debug("vr_init\n");

//	SDL_LogSetAllPriority(SDL_LOG_PRIORITY_VERBOSE);

	SDL_Init(SDL_INIT_VIDEO);

//	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
//	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_SWAP_CONTROL, 1);

	// SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	// SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);

//	vr_debug("SDL_video initialized with driver \"%s\"\n", SDL_GetCurrentVideoDriver());

	SDL_SetVideoMode(512, 512, 32, SDL_OPENGL);

//    main_window = SDL_CreateWindow("vr", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
//        512, 512, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);

//    if (main_window == NULL) /* Die if creation failed */
//    	vr_error_exit("Unable to create window: \"%s\"\n", SDL_GetError());

//	main_context = SDL_GL_CreateContext(main_window);

	GLenum glew_result = glewInit();

	if (glew_result != GLEW_OK)
		vr_error_exit("glewInit failed with \"%s\"\n", glewGetErrorString(glew_result));

	if (!GLEW_VERSION_2_1)
		vr_error_exit("OpenGL version 2.1 or higher required.\n");

	if (!GLEW_ARB_vertex_program || !GLEW_ARB_fragment_program)
		vr_error_exit("Programmable shader support required.\n");

//    SDL_GL_SetSwapInterval(1);

	return VR_OK;
}
