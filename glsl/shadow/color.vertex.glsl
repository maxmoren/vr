#version 120

varying vec4 texCoord;

void main()
{
	texCoord = gl_TextureMatrix[7] * gl_Vertex;

	gl_FrontColor = gl_Color;
	gl_Position = ftransform();
}
