#include <vr/vr.h>
#include <vr/vr_program.h>
#include <vr/vr_target.h>
#include <vr/vr_pass.h>
#include <vr/vr_texture.h>
#include <vr/vr_vector.h>
#include <vr/vr_quaternion.h>
#include <vr/vr_arcball.h>

#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef HAVE_GLES
#include <GLES2/gl2.h>
#else
#include <GL/glew.h>
#include <GL/gl.h>
#endif

static struct vr_program *raymarch_program;
static GLint uniform_mouse;
static GLint uniform_time;
static GLint uniform_resolution;
static GLfloat t = 0.0;

static int mouse_x = 0, mouse_y = 0;

void draw_quad() {
	glBegin(GL_QUADS);
		glTexCoord2f(0.0, 0.0); glVertex2f(-1.0, -1.0);
		glTexCoord2f(1.0, 0.0); glVertex2f(+1.0, -1.0);
		glTexCoord2f(1.0, 1.0); glVertex2f(+1.0, +1.0);
		glTexCoord2f(0.0, 1.0); glVertex2f(-1.0, +1.0);
	glEnd();
}

void draw() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	t += 0.1;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	vr_program_enable(raymarch_program);

	glUniform2f(uniform_resolution, 512, 512);
	glUniform2f(uniform_mouse, mouse_x, mouse_y);
	glUniform1f(uniform_time, t);

	draw_quad();
	
	vr_program_disable();
}

void setup() {
	/* Load the programs. */
	raymarch_program = vr_program_new("glsl/passthrough.vert", "glsl/foo.frag");

	if (raymarch_program == NULL) {
		vr_error("could not load shaders\n");
		exit(EXIT_FAILURE);
	}

	vr_debug("shaders loaded\n");

	uniform_mouse = vr_program_get_uniform(raymarch_program, "mouse");
	uniform_time = vr_program_get_uniform(raymarch_program, "time");
	uniform_resolution = vr_program_get_uniform(raymarch_program, "resolution");
}

void mouse(int x, int y, long buttons, void *user_data) {
	mouse_x = x;
	mouse_y = y;
}

int main(void) {
	vr_init();

	setup();

	vr_main_loop(&draw, &mouse, NULL);
	vr_exit(EXIT_SUCCESS);

	return EXIT_SUCCESS;
}
