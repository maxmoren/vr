#ifndef _FBO_H
#define _FBO_H

#include <GL/glew.h>

struct fbo;
void fbo_bind(struct fbo *);
void fbo_unbind(struct fbo *);
struct fbo *fbo_create(GLint, GLint);

void fbo_draw(struct fbo *desc, GLint x, GLint y, GLuint w, GLuint h);
void fbo_draw_depth(struct fbo *desc, GLint x, GLint y, GLuint w, GLuint h);

GLuint fbo_depth_texture(struct fbo *desc);

#endif

