#ifndef _VR_PROGRAM_H
#define _VR_PROGRAM_H

#ifdef HAVE_GLES
#include <GLES2/gl2.h>
#else
#include <GL/glew.h>
#include <GL/gl.h>
#endif

struct vr_program;

struct vr_program *vr_program_new(const char *vertex_path, const char *fragment_path);
void vr_program_reload_all(void);
void vr_program_delete(struct vr_program *program);
void vr_program_enable(struct vr_program *program);
void vr_program_disable(void);
GLint vr_program_get_uniform(struct vr_program *program, const GLchar *name);

#endif
