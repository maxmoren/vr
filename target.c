#include <vr/vr.h>
#include <vr/vr_memory.h>
#include <vr/vr_target.h>

#include <SDL.h>

struct vr_target
{
	struct vr_target *next;  /** Pointer to next target structure, or NULL */
	
	int type;               /** VR_TARGET_TEXTURE or VR_TARGET_RENDERBUFFER */

	GLuint internal_format;  /** Internal GL format to use for storage */
	GLuint object;           /** Texture or renderbuffer object */
	GLenum attachment;       /** What output this target is attached to */
};

struct vr_target *vr_target_new_texture(GLint attachment, GLsizei width, GLsizei height, GLint min_filter, GLint mag_filter)
{
	struct vr_target *target = vr_memory_alloc(sizeof(struct vr_target), 1);

	glGenTextures(1, &target->object);
	glBindTexture(GL_TEXTURE_2D, target->object);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, min_filter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mag_filter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glBindTexture(GL_TEXTURE_2D, 0);

    target->attachment = attachment;
    target->type = VR_TARGET_TEXTURE;

	return target;
}

int vr_target_get_type(struct vr_target *target) {
	return target->type;
}

GLuint vr_target_get_object(struct vr_target *target) {
	return target->object;
}

GLenum vr_target_get_attachment(struct vr_target *target) {
	return target->attachment;
}
