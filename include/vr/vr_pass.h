#ifndef _VR_PASS_H
#define _VR_PASS_H

#include <vr/vr_target.h>

struct vr_pass;

struct vr_pass *vr_pass_new(GLsizei width, GLsizei height, void (*draw_callback)(void *), void *user_data);
void vr_pass_attach_target(struct vr_pass *pass, struct vr_target *target);
void vr_pass_render(struct vr_pass *pass);

#endif
