#ifdef HAVE_GLES
#include <GLES2/gl2.h>
#else
#include <GL/glew.h>
#include <GL/gl.h>
#endif

#include "utlist.h"
#include <vr/vr.h>
#include <vr/vr_gl.h>
#include <vr/vr_memory.h>
#include <vr/vr_target.h>
#include <vr/vr_pass.h>

struct vr_pass
{
	struct vr_pass *next; /** Pointer to next pass structure, or NULL */

	GLuint framebuffer; /** Framebuffer object for this pass */
	GLsizei width, height; /** Dimensions of targets */
	struct vr_target *targets;
	void *user_data;
	void (*draw_callback)(void *);
};

/** Allocate and initialize a new render pass structure.
 *
 * @param width Width of the outputs of this render pass
 * @param height Height of the outputs of this render pass
 * @param draw_callback Function that is called when rendering, after binding framebuffers
 * @param user_data Pointer passed to draw_callback
 */
struct vr_pass *vr_pass_new(GLsizei width, GLsizei height, void (*draw_callback)(void *), void *user_data) {
	struct vr_pass *pass = vr_memory_alloc(sizeof(struct vr_pass), 1);

	pass->next = NULL;
	pass->targets = NULL;
	pass->user_data = user_data;
	pass->draw_callback = draw_callback;
	pass->width = width;
	pass->height = height;

	/* Create a FBO. */
 	glGenFramebuffers(1, &pass->framebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, pass->framebuffer);

	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

	if (status != GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT)
		vr_error_exit("FBO creation failed with \"%s\"\n", vr_gl_error(glGetError()));

	return pass;
}

/** Append a render target to this pass.
 *
 * @param pass The render pass which we would like to add this target to
 * @param target A render target structure to append
 * @returns Returns VR_OK if the target could be appended
 */
void vr_pass_attach_target(struct vr_pass *pass, struct vr_target *target) {
//	LL_APPEND(pass->targets, target);

	glBindFramebuffer(GL_FRAMEBUFFER, pass->framebuffer);
	if (vr_target_get_type(target) == VR_TARGET_TEXTURE)
		glFramebufferTexture2D(GL_FRAMEBUFFER, vr_target_get_attachment(target), GL_TEXTURE_2D, vr_target_get_object(target), 0);
	else if (vr_target_get_type(target) == VR_TARGET_RENDERBUFFER)
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, vr_target_get_attachment(target), GL_RENDERBUFFER, vr_target_get_object(target));
	else
		vr_error_exit("Unknown target type");
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

/** Render a pass.
 *
 * @param pass Pointer to a pass structure describing what to render
 */
void vr_pass_render(struct vr_pass *pass) {
	// glPushAttrib(GL_VIEWPORT_BIT);
	glViewport(0, 0, pass->width, pass->height);
	glBindFramebuffer(GL_FRAMEBUFFER, pass->framebuffer);

	pass->draw_callback(pass->user_data);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	// glPopAttrib();
}
