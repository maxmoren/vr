#include <math.h>
#include <vr/vr_vec3.h>
#include <vr/vr_math.h>

inline struct vr_vec3 vr_vec3_add(struct vr_vec3 u, struct vr_vec3 v)
{
	struct vr_vec3 r;
	r.x = u.x + v.x;
	r.y = u.y + v.y;
	r.z = u.z + v.z;
	return r;
}

inline struct vr_vec3 vr_vec3_sub(struct vr_vec3 u, struct vr_vec3 v)
{
	struct vr_vec3 r;
	r.x = u.x - v.x;
	r.y = u.y - v.y;
	r.z = u.z - v.z;
	return r;
}

inline struct vr_vec3 vr_vec3_mul(struct vr_vec3 u, struct vr_vec3 v)
{
	struct vr_vec3 r;
	r.x = u.x * v.x;
	r.y = u.y * v.y;
	r.z = u.z * v.z;
	return r;
}

inline struct vr_vec3 vr_vec3_div(struct vr_vec3 u, struct vr_vec3 v)
{
	struct vr_vec3 r;
	r.x = u.x / v.x;
	r.y = u.y / v.y;
	r.z = u.z / v.z;
	return r;
}

struct vr_vec3 vr_vec3_abs(struct vr_vec3 u)
{
	struct vr_vec3 r;
    r.x = fabs(u.x);
    r.y = fabs(u.y);
    r.z = fabs(u.z);
    return r;
}

struct vr_vec3 vr_vec3_cross(struct vr_vec3 u, struct vr_vec3 v)
{
	struct vr_vec3 r;
    r.x = u.y * v.z - u.z * v.y;
    r.y = u.z * v.x - u.x * v.z;
    r.z = u.x * v.y - u.y * v.x;
    return r;
}

struct vr_vec3 vr_vec3_norm(struct vr_vec3 u)
{
	struct vr_vec3 r = u;
    float n = vr_vec3_len(u);
    if (n)
    {
		r.x /= n;
		r.y /= n;
		r.z /= n;
    }
    return r;
}

float vr_vec3_len(struct vr_vec3 u)
{
    return sqrt(sqr(u.x) + sqr(u.y) + sqr(u.z));
}

float vr_vec3_dot(struct vr_vec3 u, struct vr_vec3 v)
{
    return u.x * v.x + u.y * v.y + u.z * v.z;
}

struct vr_vec3 vr_vec3_round(struct vr_vec3 u)
{
	struct vr_vec3 r;
	r.x = round(u.x);
	r.y = round(u.y);
	r.z = round(u.z);
	return r;
}