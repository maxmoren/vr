#ifndef __VR_QUATERNION_H
#define __VR_QUATERNION_H

#ifdef HAVE_GLES
#include <GLES2/gl2.h>
#else
#include <GL/glew.h>
#include <GL/gl.h>
#endif

struct vr_quat
{
	double w;

	double x;
	double y;
	double z;
};

void vr_quat_mat(struct vr_quat q, GLfloat *mat);
struct vr_quat vr_quat_norm(struct vr_quat q);
// struct vr_quat vr_quat_mul(struct vr_quat q, struct vr_quat r);

void vr_quat_next(GLfloat* dest, GLfloat* left, GLfloat* right);

#endif
