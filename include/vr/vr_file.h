#ifndef _VR_FILE_H
#define _VR_FILE_H

struct vr_file;

struct vr_file *vr_file_new(const char *path);
void *vr_file_ptr(struct vr_file *desc);
size_t vr_file_size(struct vr_file *desc);
void vr_file_close(struct vr_file *desc);

#endif
