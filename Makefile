CC     = gcc
CFLAGS = -ggdb -O2 -DHAVE_EGL -DDEBUG -Wall -Iinclude $(shell pkg-config --cflags glew gl sdl)
LIBS   = $(shell pkg-config --libs glew gl glu sdl) -lm

raycaster_SRCS = raycaster/raycaster.c \
	file.c \
	gl.c \
	math.c \
	memory.c \
	pass.c \
	primitive.c \
	program.c \
	target.c \
	texture.c \
	arcball.c \
	vec3.c \
	vr.c \
	quaternion.c

esltest_SRCS = esltest/esltest.c \
	file.c \
	gl.c \
	math.c \
	memory.c \
	pass.c \
	primitive.c \
	program.c \
	target.c \
	vr.c

pvmarch_SRCS = pvmarch/pvmarch.c \
	file.c \
	gl.c \
	math.c \
	memory.c \
	program.c \
	target.c \
	vec3.c \
	vr.c
pvmarch_LIBS = $(LIBS)

cameratest_SRCS = cameratest/cameratest.c \
	camera.c \
	pid.c \
	file.c \
	gl.c \
	math.c \
	memory.c \
	pass.c \
	primitive.c \
	program.c \
	target.c \
	vr.c
cameratest_LIBS = $(LIBS)

all: Makefile raycaster esltest pvmarch cameratest
	
raycaster: $(raycaster_SRCS:.c=.o)
	$(CC) $(raycaster_SRCS:.c=.o) -o raycaster/$@ $(LIBS)

esltest: $(esltest_SRCS:.c=.o)
	$(CC) $(esltest_SRCS:.c=.o) -o esltest/$@ $(LIBS)

cameratest: $(cameratest_SRCS:.c=.o)
	$(CC) $(cameratest_SRCS:.c=.o) -o cameratest/$@ $(cameratest_LIBS)

%.o: %.d

%.d: %.c
	$(CC) -M $(CFLAGS) -o $*.d $<

clean:
	$(RM) *.d *.o raycaster/raycaster esltest/esltest

-include $(raycaster_SRCS:.c=.d)

.PHONY: raycaster esltest cameratest clean
