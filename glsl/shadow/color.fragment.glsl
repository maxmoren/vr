#version 120

uniform sampler2DShadow ShadowMap;
varying vec4 texCoord;

const vec4 ambient = vec4(0.13);

void main(void)
{
	vec4 shadowValue = shadow2DProj(ShadowMap, texCoord);
	gl_FragColor = gl_Color * (shadowValue + ambient);
}
