#include <vr/vr.h>
#include <vr/vr_memory.h>
#include <vr/vr_pid.h>

struct vr_pid
{
	double kp, ki, kd;
	double prev_error;
	double integral;
};

struct vr_pid *vr_pid_new(void) {
	struct vr_pid *pid = vr_memory_alloc(sizeof(struct vr_pid), 1);

	pid->kp = 0.0;
	pid->ki = 0.0;
	pid->kd = 0.0;

	pid->prev_error = 0.0;
	pid->integral = 0.0;

	return pid;
}

void vr_pid_set(struct vr_pid *pid, double kp, double ki, double kd)
{
	pid->kp = kp;
	pid->ki = ki;
	pid->kd = kd;
}

double vr_pid_get_output(struct vr_pid *pid, double error, double dt)
{
	double derivative = (error - pid->prev_error) / dt;
	pid->integral += error * dt;
	pid->prev_error = error;

	return pid->kp * error
	     + pid->ki * pid->integral
	     + pid->kd * derivative;
}

void vr_pid_destroy(struct vr_pid *pid)
{
	vr_memory_free(pid);
}