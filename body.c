#include <vr/vr.h>
#include <vr/vr_vector.h>

enum
{
	VR_BODY_POINT,
	VR_BODY_CUBE
};

struct vr_body
{
	struct vr_body *next;
	int type;
	struct vr_vec3 pos;
	struct vr_vec3 vel;
};

struct vr_body_cube
{
	float size;
};

struct vr_body_point
{
};


struct vr_body *cubes_head;
struct vr_body player = {
		.type = VR_BODY_POINT,
		.pos = {
				.x = 0.0f,
				.y = 0.0f,
				.z = 0.0f
			},
		.vel = {
				.x = 0.0f,
				.y = 0.0f,
				.z = 0.0f
			}
	};

static const struct vr_vec3 plane_points[] = {
		{ .x  =  0.0, .y =  0.0, .z = +0.5 }, /* front */
		{ .x  =  0.0, .y =  0.0, .z = -0.5 }, /* back */
		{ .x  = +0.5, .y =  0.0, .z =  0.0 }, /* right */
		{ .x  = -0.5, .y =  0.0, .z =  0.0 }, /* left */
		{ .x  =  0.0, .y = +0.5, .z =  0.0 }, /* top */
		{ .x  =  0.0, .y = -0.5, .z =  0.0 }  /* bottom */
	};

static const struct vr_vec3 plane_normals[] = {
		{ .x  =  0.0, .y =  0.0, .z = +1.0 }, /* front */
		{ .x  =  0.0, .y =  0.0, .z = -1.0 }, /* back */
		{ .x  = +1.0, .y =  0.0, .z =  0.0 }, /* right */
		{ .x  = -1.0, .y =  0.0, .z =  0.0 }, /* left */
		{ .x  =  0.0, .y = +1.0, .z =  0.0 }, /* top */
		{ .x  =  0.0, .y = -1.0, .z =  0.0 }  /* bottom */
	};

static int block_query(int x, int y, int z)
{
	if (y == -2)
		return 1;

	return 0;
}

static struct vr_vec3 find_nearest_block(struct vr_body *body)
{
}

int vr_body_test_point(struct vr_body *point)
{
	/* Find what we will crash into. */
	struct vr_vec3 nearest = vr_vec3_add(body->pos, body->vel);

	/* Round to nearest integer away from zero, to find the coordinates of the block whose voronoi region we
	   would end up in. */
	vr_vec3_round(nearest);

	/* Check if there is anything there, or if it's empty space. */
	if (block_query(nearest.x, nearest.y, nearest.z))

	/* Check if it is too far away. */
	if (vr_vec3_len(vr_vec3_sub(cube->pos, point->pos)) > vr_vec3_len(vr_vec3_add(vr_vec3_abs(cube->vel), vr_vec3_abs(point->vel))))
		return 0;

	/* Translate the point to the cube's space. */

	return 1;
}
