#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>

#ifdef HAVE_GLES
#include <GLES2/gl2.h>
#else
#include <GL/glew.h>
#include <GL/gl.h>
#endif

#include <vr/vr.h>
#include <vr/vr_vector.h>

struct pvm_volume {
	char vol0[4];
	char vol1[4*4];
	char vol2[4*4*4];
};

static struct pvm_volume *vol;
static int mouse_x, mouse_y;

static struct vr_vec2 p0 = { 0.0, 0.0 };
static struct vr_vec2 p1 = { 0.5, 0.5 };

#define BOX_WIDTH_0 (0.25)
#define BOX_WIDTH_1 (0.50)
#define BOX_WIDTH_2 (1.00)

int ipow(int base, int exp) {
    int result = 1;
    while (exp)
    {
        if (exp & 1)
            result *= base;
        exp >>= 1;
        base *= base;
    }

    return result;
}

void set(int x, int y) {
	vol->vol2[x      + y      * 2] = 1;
	vol->vol1[(x/4)  + (y/4)  * 2] = 1;
	vol->vol0[(x/16) + (y/16) * 2] = 1;
}

int get(int x, int y, int level) {
	switch (level) {
		case 0: return vol->vol0[(x/16) + (y/16) * 2];
		case 1: return vol->vol1[(x/4)  + (y/4)  * 2];
		case 2: return vol->vol2[x      + y      * 2];
	}
}

void fill_quad(double x, double y, double r, double g, double b, double box_width) {
	x /= box_width;
	y /= box_width;

	x = floor(x) * box_width;
	y = floor(y) * box_width;

	glColor3d(r, g, b);
	glBegin(GL_QUADS);
		glVertex2f(x, y);
		glVertex2f(x + box_width, y);
		glVertex2f(x + box_width, y + box_width);
		glVertex2f(x, y + box_width);
	glEnd();

	glColor3d(1.0, 1.0, 1.0);
	glBegin(GL_LINE_STRIP);
		glVertex2f(x, y);
		glVertex2f(x + box_width, y);
		glVertex2f(x + box_width, y + box_width);
		glVertex2f(x, y + box_width);
		glVertex2f(x, y);
	glEnd();
}

void draw(void *user_data) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	struct vr_vec2 direction;
	direction = vr_vec2_sub(p1, p0);
	direction = vr_vec2_norm(direction);

	double x = p0.x, y = p0.y;

/*		glBegin(GL_LINES);
			glVertex2d(x, y);
			x += direction.x * BOX_WIDTH_2;
			y += direction.y * BOX_WIDTH_2;
			glVertex2d(x, y);
		glEnd(); */

	while (x < 1.0 && y < 1.0 && x > -1.0 && y > -1.0) {
		/* Level 2 */
		glColor3d(1.0, 0.0, 0.0);
		glBegin(GL_POINTS);
			glVertex2d(x, y);
		glEnd();
		if (get(x, y, 2)) {
			fill_quad(x, y, 0.5, 0.0, 0.0, BOX_WIDTH_2);
			x += direction.x * BOX_WIDTH_1;
			y += direction.y * BOX_WIDTH_1;
		}
		else {
			fill_quad(x, y, 0.0, 0.0, 0.5, BOX_WIDTH_2);
			x += direction.x * BOX_WIDTH_2;
			y += direction.y * BOX_WIDTH_2;
			continue;
		}

		/* Level 1 */
		glColor3d(1.0, 0.0, 0.0);
		glBegin(GL_POINTS);
			glVertex2d(x, y);
		glEnd();
		if (get(x, y, 1)) {
			fill_quad(x, y, 0.5, 0.0, 0.0, BOX_WIDTH_0);
			x += direction.x * BOX_WIDTH_0;
			x += direction.x * BOX_WIDTH_0;
		}
		else {
			fill_quad(x, y, 0.0, 0.0, 0.5, BOX_WIDTH_1);
		}
	}

	glBegin(GL_LINES);
		glColor3d(1.0, 0.0, 0.0);
		glVertex2d(p0.x, p0.y);
		glVertex2d(p1.x, p1.y);
	glEnd();

	glBegin(GL_POINTS);
		glColor3d(0.0, 1.0, 0.0);
		glVertex2d(p0.x, p0.y);
		glColor3d(0.0, 0.0, 1.0);
		glVertex2d(p1.x, p1.y);
	glEnd();
}

void setup(void) {
	glPointSize(5.0);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POINT_SMOOTH);
	glLineWidth(1.5);
	glEnable(GL_BLEND);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glHint(GL_LINE_SMOOTH_HINT, GL_DONT_CARE);
}

void mouse(int x, int y, long buttons, void *user_data) {
	mouse_x = x;
	mouse_y = y;

	if (buttons & VR_MOUSE_BUTTON1) {
		p0.x =   x / 256.0 - 1.0;
		p0.y = -(y / 256.0 - 1.0);
	}
	if (buttons & VR_MOUSE_BUTTON3) {
		p1.x =   x / 256.0 - 1.0;
		p1.y = -(y / 256.0 - 1.0);
	}
}

int main(void)
{
	vr_init();

	setup();

	vol = malloc(sizeof(struct pvm_volume));
	assert(vol);

	vr_main_loop(&draw, &mouse, NULL);
	vr_exit(EXIT_SUCCESS);

	return EXIT_SUCCESS;
}
