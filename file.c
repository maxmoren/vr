#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

#include <vr/vr.h>
#include <vr/vr_memory.h>
#include <vr/vr_file.h>

#ifdef WIN32
#define LEAN_AND_MEAN
#include <windows.h>
#else
#include <sys/mman.h>
#endif

struct vr_file
{
	void *base;
	size_t len;

	#ifdef WIN32
		HANDLE hFile;
		HANDLE hFileMappingObject;
	#else
		int fd;
	#endif
};

struct vr_file *vr_file_new(const char *path)
{
	struct vr_file *desc;

	desc = vr_memory_alloc(sizeof(desc), 1);

	#ifdef WIN32
		OFSTRUCT reOpenBuff;

		if ((int)(desc->hFile = (HANDLE)OpenFile(path,
		                                         &reOpenBuff,
		                                         OF_READ)) == -1)
		{
			vr_error("Could not OpenFile on \"%s\"\n", path);
			vr_memory_free(desc);
			return NULL;
		}

	    DWORD nFileSize = GetFileSize(desc->hFile, NULL);

		desc->hFileMappingObject = CreateFileMapping(
				desc->hFile,
				NULL,
				PAGE_READONLY,
				0,
				0,
				NULL
			);

		desc->base = MapViewOfFile(
				desc->hFileMappingObject,
				FILE_MAP_READ,
				0,
				0,
				nFileSize
			);

		if (desc->base == NULL) {
			vr_memory_free(desc);
			vr_error("MapViewOfFile returned NULL\n");
			return NULL;			
		}

		desc->len = nFileSize;

		vr_debug("Mapped %d bytes of file\n", nFileSize);

	#else
		struct stat buf;

		if ((desc->fd = open(path, O_RDONLY)) < 0)
		{
			vr_memory_free(desc);
			perror("open");
			return NULL;
		}

		if (fstat(desc->fd, &buf))
		{
			vr_memory_free(desc);
			close(desc->fd);
			perror("fstat");
			return NULL;
		}

		desc->len = buf.st_size;

		if ((desc->base = mmap(NULL, buf.st_size, PROT_READ, MAP_PRIVATE, desc->fd, 0)) == NULL)
		{
			vr_memory_free(desc);
			close(desc->fd);
			perror("mmap");
			return NULL;
		}
	#endif

	return desc;
}

void *vr_file_ptr(struct vr_file *desc)
{
	return desc->base;
}

size_t vr_file_size(struct vr_file *desc)
{
	return desc->len;
}

void vr_file_close(struct vr_file *desc)
{
	#ifdef WIN32
		UnmapViewOfFile(desc->base);
		CloseHandle(desc->hFileMappingObject);
		CloseHandle(desc->hFile);
	#else
		munmap(desc->base, desc->len);
		close(desc->fd);
	#endif

	free(desc);
}

