#include <GL/glew.h>

#include <SDL.h>
#define NO_SDL_GLEXT
#include <SDL_opengl.h>

#include <utlist.h>
#include <vr/vr.h>
#include <vr/vr_gl.h>
#include <vr/vr_memory.h>
#include <vr/vr_file.h>
#include <vr/vr_program.h>

struct vr_program
{
	struct vr_program *next;

	const char *vertex_path, *fragment_path;

	GLuint program;
	GLuint vertex_shader;
	GLuint fragment_shader;
};

static struct vr_program *programs_head;

static GLuint load_shader(const char *name, const GLchar *src, GLint src_len, GLuint type)
{
	GLuint shader;

	if ((shader = glCreateShader(type)) == 0)
	{
		vr_error("glCreateShader failed with \"%s\"\n", vr_gl_error(glGetError()));
		return 0;
	}

	glShaderSource(shader, 1, &src, &src_len);
	glCompileShader(shader);


	GLint status = GL_FALSE;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

	if (status != GL_TRUE)
	{
		char message[1024];

		memset(message, '\0', 1024);

		glGetShaderInfoLog(shader, 1024, NULL, message);
		glDeleteShader(shader);

		vr_error("Shader compilation failed for %s:\n%s\n", name, message);
		return 0;
	}

	vr_debug("Compiled shader \"%s\" successfully\n", name);

	return shader;
}

static GLuint load_program(struct vr_program *program_struct)
{
	GLuint program;
	struct vr_file *vertex_file, *fragment_file;

	if ((vertex_file = vr_file_new(program_struct->vertex_path)) == NULL ||
	    (fragment_file = vr_file_new(program_struct->fragment_path)) == NULL)
		exit(EXIT_FAILURE);

	GLuint vshader = load_shader(program_struct->vertex_path, vr_file_ptr(vertex_file), vr_file_size(vertex_file), GL_VERTEX_SHADER);
	GLuint fshader = load_shader(program_struct->fragment_path, vr_file_ptr(fragment_file), vr_file_size(fragment_file), GL_FRAGMENT_SHADER);

	vr_file_close(vertex_file);
	vr_file_close(fragment_file);

	if (vshader == 0 || fshader == 0)
		return 0;

	if ((program = glCreateProgram()) == 0)
	{
		vr_error("glCreateProgram failed with \"%s\"\n", vr_gl_error(glGetError()));
		return 0;
	}

	glAttachShader(program, vshader);
	glAttachShader(program, fshader);
	glDeleteShader(vshader);
	glDeleteShader(fshader);

	glLinkProgram(program);

	GLint status = GL_FALSE;
	glGetProgramiv(program, GL_LINK_STATUS, &status);

	if (status != GL_TRUE)
	{
		char message[1024];
		GLsizei length = 0;

		glGetProgramInfoLog(program, 1024, &length, message);
		glDeleteProgram(program);

		vr_error("Program linking failed:\n%s\n", message);
		return 0;
	}

	return program;
}

void vr_program_reload_all(void)
{
	struct vr_program *program;

	LL_FOREACH(programs_head, program)
	{
		GLuint new_program = load_program(program);

		if (new_program != 0)
		{
			glDeleteProgram(program->program);
			program->program = new_program;

			vr_debug("Reloaded program \"%s\" + \"%s\"\n", program->vertex_path, program->fragment_path);
		}
	}
}

GLint vr_program_get_uniform(struct vr_program *program, const GLchar *name)
{
	GLint location = glGetUniformLocation(program->program, name);

	if (location == -1) {
		vr_error("Could not get location of \"%s\"\n", name);
	}

	return location;
}

struct vr_program *vr_program_new(const char *vertex_path, const char *fragment_path)
{
	struct vr_program *program_struct = vr_memory_alloc(sizeof(struct vr_program), 1);

	program_struct->next = NULL;
	program_struct->vertex_path = strdup(vertex_path);
	program_struct->fragment_path = strdup(fragment_path);

	GLuint program = load_program(program_struct);

	if (program == 0)
	{
		vr_memory_free(program_struct);
		return NULL;
	}

	program_struct->program = program;

	LL_APPEND(programs_head, program_struct);

	vr_debug("Loaded program \"%s\" + \"%s\"\n", program_struct->vertex_path, program_struct->fragment_path);

	return program_struct;
}

void vr_program_delete(struct vr_program *program)
{
	glDeleteProgram(program->program);
	vr_memory_free(program);
}

void vr_program_enable(struct vr_program *program)
{
	glUseProgram(program->program);
}

void vr_program_disable(void)
{
	glUseProgram(0);
}