#ifndef __VR_TEXTURE_H
#define __VR_TEXTURE_H

#ifdef HAVE_GLES
#include <GLES2/gl2.h>
#else
#include <GL/glew.h>
#include <GL/gl.h>
#endif

struct vr_texture;

struct vr_texture *vr_texture_3d_new(const char *path, int width, int height, int depth, GLenum internal_format, GLenum stored_format, GLenum stored_type);
GLuint vr_texture_get_object(struct vr_texture *texture);

#endif
