#version 120

uniform sampler2D inside;
uniform sampler2D outside;
uniform sampler3D volume;

const float step_size = 0.01;
const vec3 light_pos = vec3(2.0, 2.0, 3.0);

vec4 transfer_func(float intensity) {
	return vec4(vec3(intensity) * 3.0, intensity);
}

float shadow(vec3 p0) {
	float result = 0.0;
	vec3 direction = normalize(light_pos - p0);
	vec3 p;
	float sum = 0.0;

	float maxLength = length(p0 - light_pos);

	for (float i = 0.0; length(i * direction) < maxLength; i += step_size) {
		p = p0 + i * direction;

		sum += texture3D(volume, p).a;

		if (sum > 0.5) {
			result = 0.4;
			break;
		}
	}

	return result;
}

vec4 composite_dvr(in vec4 current_result, in vec4 color) {
	vec4 result = current_result;

	color.a = 1.0 - pow(1.0 - color.a, step_size * 200.0);

	result.rgb += (1.0 - result.a) * color.a * color.rgb;
	result.a   += (1.0 - result.a) * color.a;

	return result;
}

vec4 composite_fh(in vec4 current_result, in vec4 color, in vec3 p) {
	vec4 result = current_result;

	if (color.a >= 0.1) {
		result.rgb = vec3(p.z);
		result.a = 1.0;
	}

	return result;
}

float rand(vec2 co) {
    return fract(sin(dot(co.xy, vec2(12.9898,78.233))) * 43758.5453);
}

void main() {
	vec3 stop  = texture2D(inside, vec2(gl_FragCoord.xy) / 512.0).xyz;
	vec3 start = texture2D(outside, vec2(gl_FragCoord.xy) / 512.0).xyz;

	if (start == stop) {
		discard;
	}

	vec3 direction = normalize(stop - start);

	float maxLength = length(start - stop);
	vec3 p = vec3(0.0);

	vec4 result = vec4(0.0);
	float alpha = 0.0;

	for (float i = 0.0; length(i * direction) < maxLength; i += step_size) {
		p = start + i * direction;

		/* Apply coordinate jittering. */
		p.z += (rand(p.xy) - 0.5) * 0.005;

		float intensity = texture3D(volume, p).a;
		vec4 color = transfer_func(intensity);

//		color.rgb -= shadow(p);

		result = composite_dvr(result, color);
		// result = composite_fh(result, color, p);

		if (color.a > 0.99)
			break;
	}

	gl_FragData[0] = result;
}
