#include <vr/vr.h>

struct vr_body_cube
{
	float size;
	float x, y, z;
	float vx, vy, vz;
};

struct vr_body_point
{
	float x, y, z;
	float vx, vy, vz;
};


struct vr_body_cube *cubes_head;
struct vr_body_point player = {  };
