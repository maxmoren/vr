#ifndef __VR_PID_H
#define __VR_PID_H

struct vr_pid;

struct vr_pid *vr_pid_new(void);
double vr_pid_get_output(struct vr_pid *pid, double error, double dt);
void vr_pid_set(struct vr_pid *pid, double kp, double ki, double kd);
void vr_pid_destroy(struct vr_pid *pid);

#endif
