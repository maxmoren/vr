#include <GL/gl.h>

#include <vr/vr_gl.h>

#define CASE_GL_ERROR(err) case err: return #err;

const char *vr_gl_error(GLenum error_value)
{
	switch (error_value)
	{
		CASE_GL_ERROR(GL_NO_ERROR)
		CASE_GL_ERROR(GL_INVALID_ENUM)
		CASE_GL_ERROR(GL_INVALID_VALUE)
		CASE_GL_ERROR(GL_INVALID_OPERATION)
		CASE_GL_ERROR(GL_STACK_OVERFLOW)
		CASE_GL_ERROR(GL_STACK_UNDERFLOW)
		CASE_GL_ERROR(GL_OUT_OF_MEMORY)
//		CASE_GL_ERROR(GL_TABLE_TOO_LARGE)
		default:
			return "Unknown error";
	}
}
